/*
 * Copyright (C) 2022  Daniel Schlieckmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Personal Database is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Morph.Web 0.1
import QtWebEngine 1.7
import Lomiri.DownloadManager 1.2
import Lomiri.Content 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'tcbase.daniel'
    automaticOrientation: true

   PageStack {
        id: mainPageStack
        anchors.fill: parent
        Component.onCompleted: mainPageStack.push(pageMain)

        Page {
            id: pageMain
            anchors.fill: parent

            WebView {
                id: mainWebView
                focus: true
                enableSelectOverride: true
                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }
                settings {
                    javascriptCanAccessClipboard: true
                    localContentCanAccessFileUrls: true
                    localContentCanAccessRemoteUrls: true
                    allowRunningInsecureContent: true
                    allowWindowActivationFromJavaScript : true
                    pluginsEnabled: true
                }
                Component.onCompleted: {
                    settings.localStorageEnabled = true;
                }

                zoomFactor: units.gu(1) / 8.4
                url: Qt.resolvedUrl('www/index.html')

                onFileDialogRequested: function(request) {
                    console.log("onFileDialogRequested");
                    request.accepted = true;
                    var importPage = mainPageStack.push(Qt.resolvedUrl("ImportPage.qml"),{"contentType": ContentType.All, "handler": ContentHandler.Source});
                    importPage.imported.connect(function(fileUrl) {
                        request.dialogAccept(String(fileUrl).replace("file://", ""));
                        mainPageStack.push(pageMain)
                    });
                }
                onNewViewRequested: {
                    request.action = WebEngineNavigationRequest.IgnoreRequest
                    if(request.userInitiated) {
                        Qt.openUrlExternally(request.requestedUrl)
                    }
                }
                onFeaturePermissionRequested: function(url, feature) {
                    grantFeaturePermission(url, feature, true);
                }
                onColorDialogRequested: function(request) {
                    request.accepted = true;
                    var colorDialog = PopupUtils.open(Qt.resolvedUrl("ColorSelectDialog.qml"), this);
                    colorDialog.defaultValue = request.color;
                    colorDialog.accept.connect(request.dialogAccept);
                    colorDialog.reject.connect(request.dialogReject);
                }
            }
		
            Component {
                id: downloadedMessage
                Popover {
                    id: popover
                    Rectangle {
                        color: "green"
                        width: popover.width
                        height: units.gu(8)

                        Label {
                            anchors.centerIn: parent
                            text: "Download completed."
                            color: "white"
                        }
                    }
                }
            }		
        }

    }
}
