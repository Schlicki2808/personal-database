function init() {
    var darkmodeSetting = localStorage.getItem("darkmode");
    if (darkmodeSetting != null) {
        darkmode = (darkmodeSetting == "1");
        if (darkmode)
            setDarkMode();
    }

    loadDatabases();
}