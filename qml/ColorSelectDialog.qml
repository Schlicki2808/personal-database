/*
 * Copyright (C) 2022  Daniel Schlieckmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Personal Database is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

Dialog {
    id: colorDialog
    objectName: "colorDialog"
    title: i18n.tr("Select Color")
    
    property string defaultValue
    property int countColors
    
    signal accept(string color)
    signal reject()
    
    onAccept: hide()
    onReject: hide()

    TextField {
        id: input
        objectName: "inputTextField"
        text: defaultValue
        onAccepted: {
            Qt.inputMethod.commit()
            accept(input.text)
        }
    }
        
    Row {
        Repeater {
            model: ["red", "orange", "gold", "yellow", "lemonchiffon"]
            Rectangle {
                width: units.gu(6)
                height: units.gu(6)
                border.width: 1
                color: modelData

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        input.text = modelData
                    }
                }
            }
        }
    }

    Row {
        Repeater {
            model: ["green", "darkseagreen", "springgreen", "palegreen", "azure"]
            Rectangle {
                width: units.gu(6)
                height: units.gu(6)
                border.width: 1
                color: modelData

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        input.text = modelData
                    }
                }
            }
        }
    }

    Row {
        Repeater {
            model: ["blue", "purple", "dodgerblue", "lightskyblue", "lavender"]
            Rectangle {
                width: units.gu(6)
                height: units.gu(6)
                border.width: 1
                color: modelData

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        input.text = modelData
                    }
                }
            }
        }
    }

    Row {
        Repeater {
            model: ["black", "gray", "lightgray", "ghostwhite", "white"]
            Rectangle {
                width: units.gu(6)
                height: units.gu(6)
                border.width: 1
                color: modelData

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        input.text = modelData
                    }
                }
            }
        }
    }


    Button {
        text: i18n.tr("OK")
        color: theme.palette.normal.positive
        objectName: "okButton"
        onClicked: {
            Qt.inputMethod.commit()
            accept(input.text)
        }
    }

    Button {
        objectName: "cancelButton"
        text: i18n.tr("Cancel")
        onClicked: reject()
    }

    /*
    Binding {
        target: model
        property: "currentValue"
        value: input.text
    }
    */
}
