# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the tcbase.daniel package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: tcbase.daniel\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-05 20:40+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/ColorSelectDialog.qml:24
msgid "Select Color"
msgstr ""

#: ../qml/ColorSelectDialog.qml:123
msgid "OK"
msgstr ""

#: ../qml/ColorSelectDialog.qml:134
msgid "Cancel"
msgstr ""

#: ../qml/ImportPage.qml:35
msgid "Choose"
msgstr ""

#: tcbase.desktop.in.h:1
msgid "Personal Database"
msgstr ""
